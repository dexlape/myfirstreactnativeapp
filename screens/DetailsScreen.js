import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

class DetailsScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text>Details Screen</Text>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  

DetailsScreen.propTypes = {

};

export default DetailsScreen;