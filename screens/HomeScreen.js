import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <ScrollView>
              <Text style={styles.title}>Home Screen</Text>
              <Button
                title="Details"
                color = "black"
                onPress={() => this.props.navigation.navigate('Details')}
              />
                <Text> </Text>
              <Button style={styles.space}
                title="Activity Indicator"
                color = "black"
                onPress={() => this.props.navigation.navigate('ActivityIndicator')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="Drawer Layout"
                color = "black"
                onPress={() => this.props.navigation.navigate('DrawerLayout')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="Image Screen"
                color = "black"
                onPress={() => this.props.navigation.navigate('ImageScreen')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="Keyboard Avoid Screen"
                color = "black"
                onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="List View"
                color = "black"
                onPress={() => this.props.navigation.navigate('ListView')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="Modal"
                color = "black"
                onPress={() => this.props.navigation.navigate('ModalScreen')}
              />
              <Text> </Text>
              <Button style={styles.space}
                title="Picker Screen"
                color = "black"
                onPress={() => this.props.navigation.navigate('PickerScreen')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Progress Bar"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('ProgressBar')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Refresh Control"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('RefreshControl')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Scroll View"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('ScrollView')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Section List"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('SectionList')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Slider"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('Slider')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Status Bar"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('StatusBar')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Switch Screen"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('Switch')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Text Input"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('TextInput')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Text Screen"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('Text')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Touchable HighLight"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('TouchableHighlight')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Touchable Native Feedback"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Touchable Opacity"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('TouchableOpacity')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="View Pager Android"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('ViewPager')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="View Screen"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('View')}
              />
              <Text> </Text>
              <Button style={styles.space}
                  title="Web View"
                  color = "black"
                  onPress={() => this.props.navigation.navigate('WebView')}
              />
              <Text> </Text>
              </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#007ACC',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 30,
      margin: 25,
      textAlign: 'center',
    },
    space: {
      margin: 20,
    }
  });

export default HomeScreen;
