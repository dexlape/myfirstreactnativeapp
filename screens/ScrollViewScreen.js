import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, Button } from 'react-native';

export default class ScrollViewScreen extends Component {
   state = {
      names: [
         {'name': 'Lea', 'id': 1},
         {'name': 'Louie', 'id': 2},
         {'name': 'Kent', 'id': 3},
         {'name': 'Lawrence', 'id': 4},
         {'name': 'Karl', 'id': 5},
         {'name': 'Lloyd', 'id': 6},
         {'name': 'Shiela', 'id': 7},
         {'name': 'Joy', 'id': 8},
         {'name': 'Hazel', 'id': 9},
         {'name': 'Ann', 'id': 10},
         {'name': 'Stephen', 'id': 11},
         {'name': 'Charles', 'id': 12}
      ]
   }
   render() {
      return (
         <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text>{item.name}</Text>
                     </View>
                  ))
               }
               <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </ScrollView>
            
         </View>
      )
   }
}

const styles = StyleSheet.create ({
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   }
});