import React, { Component } from 'react'
import { View, Switch, StyleSheet, Button } from 'react-native'

export default class SwitchScreen extends Component {
    constructor() {
        super();
        this.state = {
            switch1Value: false,
        }
    }
    toggleSwitch1 = (value) => {
        this.setState({ switch1Value: value })
        console.log('Switch 1 is: ' + value)
    }
    render() {
        return (
            <View style={styles.container}>
                <Switch
                    onValueChange={this.toggleSwitch1}
                    value={this.switch1Value} />
                    <Button style={styles.space}
                        title="Home"
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 100
    },
    space: {
        margin: 20,
    },
});